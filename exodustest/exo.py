import netCDF4 as nc
import pylab as plt

#rootgrp = nc.Dataset('out.001.e', 'r', format='NETCDF3')
rootgrp = nc.Dataset('out_034.e', 'r', format='NETCDF3')

info = dict((name, len(d)) for name, d in rootgrp.dimensions.items())

for v in sorted(rootgrp.variables):
    print v

dim    = info['num_dim']
nelem  = info['num_elem']
nnodes = info['num_nodes']

coord = []
for cd in 'xyz'[:dim]:
    coord.append(rootgrp.variables['coord%s'%cd][:])

Conn = dict()
e = 0
for i in range(info['num_el_blk']):
    elemblock = rootgrp.variables['connect%d' %(i+1)]
    elemtype = elemblock.getncattr('elem_type')
    print 'block', i
    print elemtype
    for nodes in elemblock[:]:
        Conn[e] = (elemtype, nodes)
        e += 1
stop


# x = rootgrp.variables['coordx'][:]
# y = rootgrp.variables['coordy'][:]
# z = rootgrp.variables['coordz'][:]

NodVarInfo = [''.join(v.compressed().tolist()) for v in rootgrp.variables['name_nod_var'][:]]

element_names = rootgrp.variables['elem_num_map'][:]

elemblocks = []
for i in range(info['num_el_blk']):
    conn = rootgrp.variables['connect%d' %(i+1)]
    eletype = conn.getncattr('elem_type')
    print eletype

    elemblocks.append(conn[:])
stop
NodVar = dict()
for i, name in enumerate(NodVarInfo):
    NodVar[name] = rootgrp.variables['vals_nod_var%d'%(i+1)][:].ravel()

# plt.scatter(x, y, c=NodVar['v'], cmap='hot', marker='s', s=50)
# plt.show()
# stop


for block in elemblocks[:3]:
    for conn in block[:]:
        xs = x[conn[:4]-1]
        ys = y[conn[:4]-1]
        plt.plot(xs, ys, 'r')

plt.plot(x, y, 'bo')
plt.axis('equal')
plt.show()
