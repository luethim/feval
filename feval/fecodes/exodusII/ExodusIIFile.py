from feval.FEval import *

class ExodusIIFile(object):
    """Read an ExodusII file
    """
    type = 'exodusII'

    # Dictionary of the Element types
    shapeFunctionDict = {'HEX': 'Hex8',
                         'HEX8':  'Hex8',
                         'TRI3':  'Tri3',
                         'TRI6':  'Tri6',
                         'QUAD4': 'Quad4',
                         'QUAD8': 'Quad8',
                         'QUAD9': 'Quad9',
                         'HEX27': 'Hex27',
                         }

    # inverse dictionary of the Element types
    invShapeFunctionDict = {}
    for k,v in shapeFunctionDict.items():
        invShapeFunctionDict[v] = k

    # the node pattern is from ExodusII to FEval
    nodePattern = {}
    nodePattern['Tri3']  = [0,1,2]
    nodePattern['Tri6']  = [0,1,2,3,4,5]
    nodePattern['Quad4'] = [0,1,2,3]
    nodePattern['Tet4']  = [0,1,2,3]
    nodePattern['Quad8'] = [0,1,2,3,4,5,6,7]
    nodePattern['Quad9'] = [0,1,2,3,4,5,6,7,8]
    nodePattern['Hex8']  = [0,1,2,3,4,5,6,7]
    nodePattern['Hex20'] = [0,1,2,3,4,5,6,7,8,9,10,11,16,17,18,19,12,13,14,15]
    nodePattern['Hex27'] = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,21,25,24,26,23,22,20]
    # nodePattern['Hex27'] = [0,1,2,3,4,5,6,7,8,9,10,11,16,17,18,19,12,13,14,15, 24,20,21,22,23, 25,26]

    # the inverse node pattern is from FEval to ExodusII
    nodePatternInv = {}
    nodePatternInv['Tri3']  = [0,1,2]
    nodePatternInv['Tri6']  = [0,1,2,3,4,5]
    nodePatternInv['Quad4'] = [0,1,2,3]
    nodePatternInv['Tet4']  = [0,1,2,3]
    nodePatternInv['Quad8'] = [0,1,2,3,4,5,6,7]
    nodePatternInv['Quad9'] = [0,1,2,3,4,5,6,7,8]
    nodePatternInv['Hex8']  = [0,1,2,3,4,5,6,7]
    nodePatternInv['Hex20'] = [0,1,2,3,4,5,6,7,8,9,10,11,16,17,18,19,12,13,14,15]
    nodePatternInv['Hex27'] = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 26, 20, 25, 24, 22, 21, 23]
    # nodePatternInv['Hex27'] = [0,1,2,3,4,5,6,7,8,9,10,11,16,17,18,19,12,13,14,15, 21,22,23,24,20, 25,26]

    # the side pattern is from ExodusII to FEval
    sidePattern = {}
    sidePattern['Quad4']    = [1, 2, 3, 4]
    sidePattern['Hex8']    = [1, 2, 3, 4, 0, 5]

    # the side pattern is from FEval to ExodusII
    sidePatternInv = {}
    sidePatternInv['Hex8'] = [4, 0, 1, 2, 3, 5]
    sidePatternInv['Quad4'] = [0, 1, 2, 3]

    # nodes = np.take( c[4:], self.nodePattern[ elemtype ] )

    # sidenodes = {}
    # ExodusII  sidenodes
    # sidenodes['Hex8'] = [[0, 1, 5, 4], 
    #                      [1, 2, 6, 5],
    #                      [2, 3, 7, 6],
    #                      [0, 4, 7, 3],
    #                      [0, 3, 2, 1],
    #                      [4, 5, 6, 7]]
    ## Feval sidenodes
    # sidenodes   = np.array(
    #               [[0,3,2,1],
    #                [0,1,5,4],
    #                [1,2,6,5],
    #                [2,3,7,6],
    #                [3,0,4,7],
    #                [4,5,6,7],
    #                ])



    def __init__(self, model):
        self.FileName = ''
        self.model    = model

    def readFile(self, filename, step=0, updatemodel=True):
        """Read the Exodus file
        """
        import netCDF4

        self.FileName = filename
        rootgrp = netCDF4.Dataset(filename, 'r', format='NETCDF3')
        rootgrp.set_auto_mask(False)   # we don't want masked arrays

        self.info = dict((name, len(d)) for name, d in rootgrp.dimensions.items())
        self.dim    = self.info['num_dim']
        self.nelem  = self.info['num_elem']
        self.nnodes = self.info['num_nodes']
        self.nsteps = self.info['time_step']
        self.model.dim = self.dim

        if step > self.nsteps-1:
            print('step bigger than %d ' % (self.nsteps-1))
            # return

        # read the node and element numbers maps
        try:
            self.node_num_map = rootgrp.variables['node_num_map'][:]
        except:
            pass
        try:
            self.elem_num_map = rootgrp.variables['elem_num_map'][:]
        except:
            pass

        # read the time steps
        self.time = []
        if 'time_whole' in rootgrp.variables:
            self.time = rootgrp.variables['time_whole'][:]

        # read the coordinates
        coords = []
        if 'coordx' in rootgrp.variables:
            for cd in 'xyz'[:self.dim]:
                coords.append(rootgrp.variables['coord%s'%cd][:])
        else:
            coords = rootgrp.variables['coord'][:]
        for i, coord in enumerate(np.vstack(coords).T):
            try:
                cn = self.node_num_map[i]
            except:
                cn = i
            self.model.Coord[cn] = coord

        # read the element connectivity
        i = 0   # count the elements by hand, as the indices have to run through all blocks
        for iblock in range(self.info['num_el_blk']):
            elemblock = rootgrp.variables['connect%d' %(iblock+1)]
            elemtypename = self.shapeFunctionDict[elemblock.getncattr('elem_type')]
            for nodes in elemblock[:]:
                try:
                    en = self.elem_num_map[i]
                except:
                    en = i
                i += 1
                self.model.Conn[en] = (elemtypename, nodes)

        # read the nodal variables
        if 'name_nod_var' in rootgrp.variables:
            self.model.NodVarInfo = [b''.join(v).decode().replace('\x00', '') 
                                     for v in rootgrp.variables['name_nod_var'][:]]

            nodvars = []
            for ivar in range(self.info['num_nod_var']):
                nodvars.append(rootgrp.variables['vals_nod_var{:d}'.format(ivar+1)][:])
            nodvars = np.array(nodvars)
        
            self.model.NodVar = dict()
            for i in range(nodvars.shape[2]):
                nn = self.node_num_map[i]
                self.model.NodVar[nn] = nodvars[:,:,i]

        # read the element variables
        if 'name_elem_var' in rootgrp.variables:
            self.model.EleVarInfo = [b''.join(v).decode().replace('\x00', '') 
                                     for v in rootgrp.variables['name_elem_var'][:]]

            elevars = []
            for ivar in range(self.info['num_elem_var']):
                evars = []
                for iblock in range(self.info['num_el_blk']):
                    evars.append(rootgrp.variables['vals_elem_var{:d}eb{:d}'.format(ivar+1, iblock+1)][:])
                elevars.append(np.hstack(evars))

            elevars = np.array(elevars)

            self.model.EleVar = dict()
            for i in range(elevars.shape[2]):
                en = self.elem_num_map[i]
                self.model.EleVar[en] = elevars[:,:,i]
        
        # load the node sets
        num_node_sets = self.info.get(u'num_node_sets', 0)
        if num_node_sets > 0:
            setids = rootgrp.variables['ns_prop1'][:]
            for n in range(num_node_sets):
                key = 'node_ns{:d}'.format(n+1)
                if key in rootgrp.dimensions:
                    nodes = rootgrp.variables[key][:]
                    self.model.setSet('node', 'ns{:d}'.format(n+1), setids[n], nodes)
        # load the element sets
        # num_elem_sets = self.info.get(u'num_el_blk',0)
        # setids = rootgrp.variables['eb_prop1'][:]
        # for n in range(num_elem_sets):
        #     elems = rootgrp.variables['elem_es%d' % (n+1)][:]
        #     self.model.setSet('elem', 'es%d' % (n+1), setids[n], elems)
        # load the side sets
        num_side_sets = self.info.get(u'num_side_sets', 0)
        if num_side_sets > 0:
            setids = rootgrp.variables['ss_prop1'][:]
            sidePattern = self.sidePattern['Hex8']
            for n in range(num_side_sets):
                elems = rootgrp.variables['elem_ss%d' % (n+1)][:]
                sides = rootgrp.variables['side_ss%d' % (n+1)][:]
                # setname = b''.join(rootgrp.variables['ss_names'][n])
                setname = b''.join(rootgrp.variables['ss_names'][n]).decode().replace('\x00', '')
                if len(setname) == 0:
                    setname = 'set{}'.format(n)
                # Exodus is 1-based
                sidestr= [sidePattern[s-1] for s in sides]
                self.model.setSet('side',  setname, setids[n], [elems, sidestr])

        # close the ExodusII file
        rootgrp.close()
        
        # finally update the model
        if updatemodel:
            self.model.update()


    def writeFile(self, filename, maximum_name_length=32):
        """Write the model to an ExodusII file
        """
        import netCDF4
        self.FileName = filename

        rootgrp = netCDF4.Dataset(filename, 'w', format='NETCDF3_CLASSIC')
        rootgrp.api_version = 5.22
        rootgrp.version = 5.22
        rootgrp.floating_point_word_size = 8
        rootgrp.file_size = 1
        rootgrp.int64_status = 0
        rootgrp.title = filename
        rootgrp.maximum_name_length = maximum_name_length
        maxname = maximum_name_length +1
        namefmt = '%'+'-%ds' % maxname
        numnodvar = len(self.model.NodVarInfo)

        rootgrp.createDimension(u'len_string', maxname)
        rootgrp.createDimension(u'len_line', 81)
        rootgrp.createDimension(u'four', 4)
        rootgrp.createDimension(u'time_step', 0)
        rootgrp.createDimension(u'len_name', maxname)
        rootgrp.createDimension(u'num_dim', self.model.dim)
        rootgrp.createDimension(u'num_nodes', len(self.model.Coord))
        rootgrp.createDimension(u'num_elem', len(self.model.Conn))
        rootgrp.createDimension(u'num_el_blk', 1)
        rootgrp.createDimension(u'num_el_in_blk1', len(self.model.Conn))
        rootgrp.createDimension(u'num_nod_per_el1', len(next(iter(self.model.Conn.values()))[1]))
        if numnodvar > 0:
            rootgrp.createDimension(u'num_nod_var', len(self.model.NodVarInfo))

        var = rootgrp.createVariable('time_whole', 'd', ('time_step'))
        var[:] = [0]
        var = rootgrp.createVariable('eb_status', 'i', ('num_el_blk'))
        var[:] = (1)
        var = rootgrp.createVariable('eb_prop1', 'i', ('num_el_blk'))
        var[:] = (1)
        var.setncattr('name', 'ID')
        var = rootgrp.createVariable('eb_names', 'c', ('num_el_blk', 'len_name'))
        var[:] = (namefmt%'eleblock1')
        var = rootgrp.createVariable('coor_names', 'c', ('num_dim', 'len_name'))
        var[:] = (namefmt%'x', namefmt%'y', namefmt%'z')[:self.model.dim]

        # write node variable values
        if numnodvar > 0:
            var = rootgrp.createVariable('name_nod_var', 'c', ('num_nod_var', 'len_name'))
            var[:] = [namefmt%n for n in self.model.NodVarInfo]
            nodvars = np.array([self.model.NodVar[k] for k in sorted(self.model.NodVar)])
            for nv in range(numnodvar):
                var = rootgrp.createVariable('vals_nod_var%d'%(nv+1), 'd', ('time_step', 'num_nodes'))
                var[:] = nodvars[:,nv][np.newaxis,:]

        var = rootgrp.createVariable('node_num_map', 'i', ('num_nodes'))
        var[:] = [i+1 for i, node in enumerate(self.model.getCoordNames())]
        var = rootgrp.createVariable('elem_num_map', 'i', ('num_elem'))
        var[:] = [i+1 for i, elem in enumerate(self.model.getElementNames())]

        # write the coordinates
        for i, cd in enumerate('xyz'[:self.model.dim]):
            var = rootgrp.createVariable('coord%s'%cd, 'd', ('num_nodes'))
            var[:] = [self.model.Coord[node][i] for node in self.model.getCoordNames()]

        var = rootgrp.createVariable('connect1', 'i', ('num_el_in_blk1', 'num_nod_per_el1'))
        var.elem_type = 'HEX8'
        var[:] = np.array([self.model.Conn[elem][1] for elem in self.model.getElementNames()])

        # write elem sets
        if 'elem' in self.model.Sets:
            rootgrp.createDimension('num_elem_sets', len(self.model.Sets['elem']))
            var = rootgrp.createVariable('es_names', 'c', ('num_elem_sets', 'len_name'))
            var[:] = [namefmt %name for name in self.model.Sets['elem'].keys()]
            for i in range(len(self.model.Sets['elem'])):
                var = rootgrp.createVariable('es_prop%d' %(i+1), 'i', 'num_elem_sets')
            var[:] = [stuff[0] for stuff in self.model.Sets['elem'].values()]
            var.setncattr('name', 'ID')

            for i, name in enumerate(self.model.Sets['elem'].keys()):
                setid, elems = self.model.Sets['elem'][name]
                sid = 'es%d' %(i+1)
                numelem = 'num_elem_%s' %sid
                rootgrp.createDimension(numelem, len(elems))
                var = rootgrp.createVariable('elem_%s' %sid, 'i', (numelem))
                var[:] = elems

        # write node sets
        if 'node' in self.model.Sets:
            rootgrp.createDimension('num_node_sets', len(self.model.Sets['node']))
            var = rootgrp.createVariable('ns_names', 'c', ('num_node_sets', 'len_name'))
            var[:] = [namefmt %name for name in self.model.Sets['node'].keys()]
            var = rootgrp.createVariable('ns_prop1', 'i', 'num_node_sets')
            var[:] = [stuff[0] for stuff in self.model.Sets['node'].values()]
            var.setncattr('name', 'ID')

            for i, (name, (sid, nodes)) in enumerate(self.model.Sets['node'].items()):
                sid = 'ns%d' %(i+1)
                numnode = 'num_nod_%s' %sid
                rootgrp.createDimension(numnode, len(nodes))
                var = rootgrp.createVariable('node_%s' %sid, 'i', numnode)
                var[:] = nodes

        # write side sets
        if 'side' in self.model.Sets:
            nsidesets = len(self.model.Sets['side'])
            rootgrp.createDimension('num_side_sets', nsidesets)
            var = rootgrp.createVariable('ss_names', 'c', ('num_side_sets', 'len_name'))
            var[:] = [namefmt %name for name in self.model.Sets['side'].keys()]
            var = rootgrp.createVariable('ss_status', 'i', 'num_side_sets')
            var[:] = np.ones(nsidesets)
            var = rootgrp.createVariable('ss_prop1', 'i', 'num_side_sets')
            var[:] = [stuff[0] for stuff in self.model.Sets['side'].values()]
            var.setncattr('name', 'ID')

            sidePatternInv = self.sidePatternInv['Hex8']
            # sidePatternInv = self.sidePatternInv['Quad4']
            for i, (name, (setid, (elems, sides))) in enumerate(self.model.Sets['side'].items()):

            # for i, name in enumerate(self.model.Sets['side']):
            #     setid, elems, sides = self.model.Sets['side'][name]
                sid = 'ss%d' %(i+1)
                numside = 'num_side_%s' %sid
                rootgrp.createDimension(numside, len(elems))
                var = rootgrp.createVariable('elem_%s' %sid, 'i', (numside))
                var[:] = elems
                var = rootgrp.createVariable('side_%s' %sid, 'i', (numside))
                # +1 since Exodus is 1-based
                var[:] = [sidePatternInv[s]+1 for s in sides]


                
        # close the ExodusII file
        rootgrp.close()
        
        # finally update the model
        self.model.update()

    def close(self):
        pass

### Test

if __name__ == '__main__':

    # # write a mesh with nodvars
    # m = FEModel()
    # from feval.Quadmesh import makeSquare
    # makeSquare(m, 0, 3, 0, 1, 3, 2)
    # # add dummy nodvars
    # m.NodVarInfo = ['xcoord', 'cy']
    # for n in m.Coord:
    #     m.NodVar[n] = np.array([m.Coord[n][0], m.Coord[n][1]])
    # m.renumberNodes()
    # m.renumberElements()
    # gf = ExodusIIFile(m)
    # gf.writeFile('nodvar.e')

    # stop

    # filename = '../../../data/mug.e'
    # filename = '../../../data/out_elastic_elevar.e'
    # filename = '../../../data/testexodusfile.e'
    # filename = '../../../exodustest/simple.e'

    # filename = '/home/tinu/projects/moose/sample_compression/combined_creep_plasticity_out.e'
    # filename = '/data/soft/moose-projects/moose/examples/ex01_inputfile/mug.e'

    # filename = '/home/tinu/projects/moose/schuelberg/s.e'
    filename = '/home/tinu/projects/moose/rhone_rock/rhone_out.e'

    m = FEModel()
    gf = ExodusIIFile(m)
    gf.readFile(filename, step=1)

    m.NodVar = dict()
    m.NodVarInfo = dict()
    gf.writeFile('a.e')
    stop

    import pylab as plt
    for n in m.Coord:
        cx, cz = m.Coord[n][:2]
        vx, vz, p = m.NodVar[n]
        dt = 0.3
        plt.plot(cx, cz, 'rs')
        plt.plot([cx, cx+vx*dt], [cz, cz+vz*dt], 'r-')
        plt.text(cx, cz, n, color='k')

    for e in m.Conn:
        t, nodes = m.Conn[e]
        cc = [m.Coord[n] for n in nodes[:3]]
        cc.append(m.Coord[nodes[0]])
        cc = np.array(cc)
        plt.plot(cc[:,0], cc[:,1], 'k-')

    plt.show()

    
    # from feval.Quadmesh import makeSquare
    # makeSquare(m,
    #            0., 1.,
    #            0., 2.,
    #            5, 6)
    # m.dim = 2
    # m.renumberNodes(1)
    # m.renumberElements(1)

    # gf = ExodusIIFile(m)
    # gf.readFile(filename, step=0)
    # m.update()
    # m.renumberNodes(1)
    # m.renumberElements(1)
    # m.update()

    # m.setSet('elem', 'elemtestset1', 1, [1, 3, 5, 7])
    # # m.setSet('elem', 'elemtestset2', [2, 4, 6, 8])
    # # m.setSet('node', 'testset1', [1, 3, 5, 7])
    # # m.setSet('node', 'testset2', [2, 4, 6, 8])
    # gf.writeFile('new.e')

    # stop


    # gf.readFile(filename, step=0)

    # stop
    # nodvarinfo = m.NodVarInfo[:2]
    
    # coord = []
    # vals  = []
    # for n in sorted(m.getNodeNames()):
    #     c = m.Coord[n]
    #     if -10 <= c[1] <= 10:
    #         coord.append(c)
    #         vals.append(m.NodVar[n])
    # coord = np.array(coord)
    # plt.scatter(coord[:,0], coord[:,2], c=vals, cmap='hot', marker='s', s=50)
    # plt.axis('equal')
    # plt.show()

    # stop

