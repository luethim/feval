# the GMSH file reader/writer
# http://geuz.org/gmsh/doc/texinfo/gmsh.html#MSH-ASCII-file-format

import re
import numpy as np
from feval.FEval import *
from feval.FETextFile import *

class GMSHFile(FETextFile):
    """Parse a GMSH input-file
    Take care of the node pattern which is different from the MARC node
    pattern which  is applied throughout these routines.
    (The MARC node pattern is: first the corner nodes, then the side
    nodes, the Femtool pattern is corner-side-corner-side...)

    The node pattern is saved in the list |Variables|
    """
    type = 'gmsh'

    sections = ['nod', 'elm']

    # Dictionary of the Element types
    shapeFunctionDict = { 1: 'Line2', \
              2: 'Tri3',
              3: 'Quad4',
              4: 'Tet4',
              5: 'Hex8',
              6: 'Prism6',
              7: 'Pyram5',
                          8: 'Line3',
                          9: 'Tri6',
                         15: 'Point1',
                          }

    # inverse dictionary of the Element types                               
    elementTypeDict = {}                                                    
    for k,v in shapeFunctionDict.items():                                   
        elementTypeDict[v] = k
        
    # the node pattern is from GMSH to Marc
    nodePattern = {}
    nodePattern['Tri3']  = [0,1,2]
    nodePattern['Quad4'] = [0,1,2,3]
    nodePattern['Tet4']  = [0,1,2,3]
    nodePattern['Quad8'] = [0,2,4,6,1,3,5,7]
    nodePattern['Hex8']  = [0,1,2,3,4,5,6,7]
    nodePattern['Hex8']  = [0,1,2,3,4,5,6,7]

    # the inverse node pattern is from Marc to GMSH
    nodePatternInv = {}
    nodePatternInv['Tri3']  = [0,1,2]
    nodePatternInv['Quad4'] = [0,1,2,3]
    nodePatternInv['Tet4']  = [0,1,2,3]
    nodePatternInv['Quad8'] = [0,4,1,5,2,6,3,7]
    nodePatternInv['Hex8']  = [0,1,2,3,4,5,6,7]

    def __init__(self, model):
        FETextFile.__init__(self, model)
        self.sections = self.sectionsStd

        # Pairs of characters used to mark comments
        self.Comment = [( '#', '\n' )] 

    def isKeyword(self, line):
        """check wether the line could contain a magic word
        return None if the line is not interesting
        In GMSH the magic words start with "$", so look for this
        """
        if line[0] == '$':
            return 1, line[1:].lower()
        else:
            return 0, line

    def extract_nodes(self, linelist):
        self.extract_nod(linelist)

    def extract_nod(self, linelist):
        """
        omit the first and the last line which contain $ELM and $ENDELM
        """
        linelist = linelist[2:]
        for line in linelist:
            words = line.split()
            id = int(words[0])
            c = np.array([float(x) for x in words[1:]])
            self.model.setCoord( id, c )

    def compose_nod(self):
        lines = []
        lines.append('$NOD\n')
        ckeys = sorted(self.model.Coord.keys())
        lines.append('%i\n' % len(ckeys))
        for id in ckeys:
            coord = self.model.Coord[id]
            ndim = len(coord)
            line = '%5i' % (id) + ' %f'*ndim \
                   % tuple(coord.tolist()) + '\n'
            lines.append(line)
        lines.append('$ENDNOD\n')
        return lines

    def extract_elements(self, linelist):
        self.extract_elm(linelist)


    def extract_elm(self, linelist):
        linelist = linelist[2:]
        for line in linelist:
            words = [int(x) for x in line.split()]
            id = words[0]
            c = np.array(words[1:])
            try: 
                elemtype = self.shapeFunctionDict[c[0]]
            except:
                elemtype = ''
                print('**** Element type %i not defined!' % (c[0]))
            nodes = np.take( c[4:], self.nodePattern[ elemtype ] )
            self.model.setElement( id, elemtype, nodes )

    def compose_elm(self):
        # assemble a dictionary with the set ids of the elements
        elemsets = dict()
        if 'elem' in self.model.Sets:
            for id, elems in self.model.Sets['elem'].items():
                for elem in elems:
                    elemsets.setdefault(elem, []).append(id)

        lines = []
        lines.append('$ELM\n')
        ckeys = sorted(self.model.Conn.keys())
        lines.append('%i\n' % len(ckeys))
        for id in ckeys:
            conn = self.model.Conn[id]
            nodes = conn[1]
            nodes = np.take(nodes, self.nodePatternInv[ conn[0] ] )
            type = self.elementTypeDict[conn[0]]
            nnod = len(nodes)
            # check if the element is part of a set, if yes, take the first set
            # since GMSH only allows one set per element
            setid = 1
            if id in elemsets:
                setid = elemsets[id][0]
            line = '%5i%5i%5i%5i%5i' % (id, type, setid, 1, nnod)
            ll = '%7i'*nnod % tuple(nodes)
            lines.append(line+ll+'\n')
        lines.append('$ENDELM\n')
        return lines

### Test

if __name__ == '__main__':

    m = FEModel()
    ff = GMSHFile(m)
    #infilename  = '/home/tinu/projects/wrangell/libmesh/sphere.msh'
    infilename  = '../../../examples/mesh.gmsh'
    ff.readFile(infilename)

    ff.setWrite('nod')
    ff.setWrite('elm')

#    import pdb; pdb.set_trace()
    ff.writeFile('out.gmsh')


