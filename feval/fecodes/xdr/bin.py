import numpy as N
import struct


f = open('a.binary', 'wb')
f.write(struct.pack('!L', 6))
f.write('LIBM 0\x00\x00')
f.write(struct.pack('!6L', 10, 11, 52, 2, 65536, 2))
f.close()



stop


bytes = open('/home/tinu/projects/libmesh/ex2/a2.xdr','rb').read()

filetype, level = bytes[4:8], bytes[9]
if not filetype == 'LIBM':
    print('file type not (yet) supported', filetype)
    stop

nelems, nnodes, sumweight, bcond, x, neleblocks = N.fromstring(bytes[12:36], '>i')
eletypes = N.fromstring(bytes[36:36+4*neleblocks], '>i')
elenums  = N.fromstring(bytes[36+4*neleblocks:36+8*neleblocks], '>i')

ptr = 36+8*neleblocks +32

conn = N.fromstring(bytes[ptr:ptr+4*sumweight], '>i')
ptr += 4*sumweight

coord = N.fromstring(bytes[ptr:ptr+8*3*nnodes], '>d')
coord.shape = ( len(coord)//3, 3)
ptr += 8*3*nnodes

