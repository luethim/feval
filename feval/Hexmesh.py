# make a boring hex mesh

import numpy as N
import feval.FEval

def makeCube(model, xmin, xmax, ymin, ymax, zmin, zmax, nx, ny, nz):
    """creates a cube mesh of nx*ny*nz elements
    """
    for i in range(nx+1):
        for j in range(ny+1):
            for k in range(nz+1):
                model.setCoordinate('%d,%d,%d' %(i,j,k), [xmin + (xmax-xmin)/float(nx)*i,
                                                      ymin + (ymax-ymin)/float(ny)*j,
                                                      zmin + (zmax-zmin)/float(nz)*k])
                if i < nx and j < ny and k < nz:
                    corners = ['%d,%d,%d' % (i  ,j  ,k  ),
                               '%d,%d,%d' % (i+1,j  ,k  ),
                               '%d,%d,%d' % (i+1,j+1,k  ),
                               '%d,%d,%d' % (i  ,j+1,k  ),
                               '%d,%d,%d' % (i  ,j  ,k+1),
                               '%d,%d,%d' % (i+1,j  ,k+1),
                               '%d,%d,%d' % (i+1,j+1,k+1),
                               '%d,%d,%d' % (i  ,j+1,k+1),
                               ]
                    model.setElement('%d,%d,%d' %(i,j,k), 'Hex8', corners)

def test():
    m = feval.FEval.FEModel()
    makeCube(m,
             0., 1.,
             0., 2.,
             0., 4.,
             20, 20, 10)
    

if __name__ == '__main__':
    m = feval.FEval.FEModel()
    makeCube(m,
             0., 1.,
             0., 2.,
             0., 4.,
             20, 20, 10)

    m.renumberNodes()
    m.renumberElements()

    import feval.fecodes.gmv.GMVFile as gmv
    mf = gmv.GMVFile(m)

    mf.setWrite('gmvinput')
    mf.setWrite('nodes')
    mf.setWrite('cells')
    mf.setWrite('variable')
    mf.setWrite('endgmv')

    mf.writeFile('adaptive.gmv.000')
    
