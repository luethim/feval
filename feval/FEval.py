# -*- coding: iso-8859-1 -*-

#============================================================================
#
#           This file is part of FEval, a module for the
#           evaluation of Finite Element results
#
# Licencse: FEval is provided under the GNU General Public License (GPL)
#
# Author:   Martin L�thi, tnoo@tnoo.net
#
# Homepage: http://www.sourceforge.net/projects/feval
#
# History:  2001.07.02 (ml):  Improved findNextElement. Performance 
#                             improved by 15% (2d) to 80% (3d) 
#           2001.09.21 (ml):  Code cleaned up for intial public release
#           2002.05.17 (ml):  Use Scipy instead of Multipack
#           2002.07.31 (ml):  Improved version of findModelBoundary
#           2002.08.08 (ml):  getIntPointVar works
#           2002.08.10 (ml):  all methods now allow to give coordinates as
#                             tuples or lists
#           2016.12.16 (ml):  refactoring to python3
#  
# Todo:     o findModelBoundary (make it work for outside points)
#           o getIntPointVar (there might still be some problems)
#           o handle case when scipy is not available (Newton algorithm)
#============================================================================

import numpy as np
from feval.ShapeFunctions import shapeFunctions
from feval.ModelData import ModelData
import feval.Element as Element
from collections import defaultdict

# a convenience function, should be faster! 
def isInRange( val, bound ):
    """Returns true if all components of the array |val| are less than |bound|"""
    return np.alltrue( np.less_equal( np.absolute( val ), bound ) )

class FEModel(ModelData):

    def __init__(self, name='FE-Model', verbose=0):
        """
        |accuracy| is the accuracy of local coordinates
        |elem_tol| is the tolerance of the local coordinates and lets
        the user adjust the sloppyness of evaluation
        o if this value is exceeded, a new element is sought
        o if the element is at the boundary (no adjacent element
            exists), the element is returned, if the local coordinates
            within the elem_btol
        |elem_btol| is the tolerance of the model boundary,
        if the local coordinates + elem_btol is exceeded, the point
        is considered outside of the model
        """
        ModelData.__init__(self, name)
        self.elementMidPoints = None
        # reset all cached results
        self.nodeIndex = {}
        self.boundaryElems = {}
        self.boundaryNodes = {}

        self.accuracy = 1.49012e-8
        self.elem_tol = self.elem_btol = 0.0001
        self.makeModelCache()
        self.verbose = verbose

    def update(self):
        self.makeModelCache()

    def makeModelCache(self):
        """Create caches for model quantities to allow fast lookups 
        o an array with the mid-point coordinates of all elements
        o nodeIndex     = key: node    | value: list of elements containing node
                          (this is the inverse of the connectivity self.Conn), 
                          but also contains loose nodes (not in any connectivity)
        o sideIndex     = key: ordered side nodes | value: list of elements
        o neighborIndex = key: element | value: list of adjacent elements or None
        o boundaryElems = key: element | value: list of boundary sides and of boundary nodes
        o boundaryNodes = key: node    | value: list of boundary element and side containing the node

        Reset the element variables (this is useful when a new
        timestep is read)""" 
        # reset all cached results
        self.elementCache  = {None: None}  # dummy entry
        self.shapeCache    = {None: None}  # dummy entry
        self.nodeIndex     = defaultdict(list)     # 
        self.sideIndex     = defaultdict(list)
        self.sideNodeIndex = defaultdict(list)
        self.neighborIndex = defaultdict(list)     
        self.neighborSideIndex = defaultdict(list)
        self.boundaryElems = defaultdict(list)
        self.boundaryNodes = defaultdict(list)
        midPoints = []

        # sanity check: are there any Nodes or Elements
        if (len(self.Conn) == 0) and (len(self.Coord) == 0):
            return

        print('============== build model cache')

        # create the nodeIndex bounding box
        mincoord = next(iter(self.Coord.values()))
        maxcoord = mincoord.copy()

        nonelist = [None]*20
        for elem, (shape, conn) in self.Conn.items():
            for n in conn:                           # loop over the nodes of the element
                mincoord = np.minimum(mincoord, self.Coord[n])
                maxcoord = np.maximum(maxcoord, self.Coord[n])
                self.nodeIndex[n].append(elem)
            sh = shapeFunctions[shape]
            self.neighborIndex[elem] = nonelist[:sh.nsides]   # initialize the neighborIndex with Nones
            for s, sn in enumerate(sh.sidenodes):
                ns = np.take(conn, sn)
                # create a unique key per element (sorted tuple)
                self.sideIndex[tuple(sorted(ns))].append((elem, s, ns))
                for n in ns:
                    self.sideNodeIndex[n].append((elem, s))

        for nodes, es in self.sideIndex.items():
            if len(es) == 2:
                e0, e1 = es[:2]
                self.neighborIndex[e0[0]][e0[1]] = e1[0]
                self.neighborIndex[e1[0]][e1[1]] = e0[0]
            else:
                e = es[0]
                self.boundaryElems[e[0]].append((e[1], e[2]))
                for n in e[2]:
                    self.boundaryNodes[n].append((e[0], e[1]))

        # self.elementMidPoints = np.asarray( midPoints, dtype=np.float_ )
        self.boundingbox      = np.asarray([mincoord, maxcoord])
        self.dirty['Conn'] = False
        self.dirty['Coord'] = False
        self.dirty['Var'] = False
        
    def removeUnusedNodes(self):
        """remove unused Nodes
        we should do this on creating the model cache
        maybe this should be called on reading
        """
        usednodes = set()
        for conn in self.Conn.values():
            usednodes.update(set((conn[1])))
        for node in self.Coord.keys():
            if not node in usednodes:
                self.Coord.pop(node)

    def findClosestNode(self, coord, recalc=True):
        """Find the name of the node closest to |coord|.
        the key of the node-dictionary is returned
        """
        allcoord = self.getAllCoordinatesAsArray()
        idx = np.sum((allcoord - coord)**2, axis=1).argmin()
        return list(self.Coord.keys())[idx]

    def findClosestElement(self, coord):
        """Find the name of the element closest to |coord|.
        the key of the element-dictionary is returned
        """
        dc = self.elementMidPoints - coord
        #eidx = np.argmin( np.sum( dc*dc, 1) )
        eidx = (dc*dc).sum(axis=1).argmin()
        ekey = list(self.Conn.keys())[eidx]
        return self.getElement(ekey)

    def findElement(self, coord, accuracy=1.49012e-8):
        """Find the element containing the global coordinate |coord|
        """
        # fast strategy
        n = self.findClosestNode(coord)
        for elem in self.nodeIndex[n]:
            e = self.getElement(elem)
            if e.containsPoint(coord, accuracy):
                return e

        # if the fast strategy did not work, use the slow one
        e = self.findClosestElement(coord)
        e_old = None
        lc = e.findLocalCoord(coord, self.accuracy)
        pingpongcount = 0
        while e and not isInRange( lc, 1.0+self.elem_tol):
            ee = self.findNextElement( e, lcoord = lc )
            if pingpongcount > 20:
                return None
            # if new element is within the model
            # test for element ping-pong loop
            if ee != None and e_old != None:
                print('ping-pong: ', ee.name, e_old.name)
                pingpongcount += 1
            if ee and ee != e_old:
                lc = ee.findLocalCoord(coord, self.accuracy)
                e_old = e
                e = ee
            # if no new element exists (= boundary)
            else:
                lc = e.findLocalCoord(coord, self.accuracy)
                if not isInRange( lc, 1.0+self.elem_btol):
                    e = None
                    if self.verbose:
                        print("The point is not within the model: ", coord)
                else:
                    return e
        return e

    def findElementsFromNodes(self, nodelist):
        """Find the elements containing two of the nodes in the |nodelist|,
        returns a list of (element, face-number) pairs, or []"""
        elems = {}
        for n in nodelist:
            ee = self.nodeIndex[n]
            for e in ee:
                e.setdefault(e, []).append(n)

        elemsides = []
        for e, nodes  in elems.items():
            if len(nodes) > 1:
                elem = self.getElement( e )
                # take the corner nodes
                cn = list( np.array(elem.nodes)[elem.shape.cornernodes] )
                cn.append(cn[0])
                for side in range(len(cn)-1):
                    if cn[side] in nodes and cn[side+1] in nodes:
                        elemsides.append( (e, side) )
        elemsides.sort()
        return elemsides

    def findNextElement(self, actelem, sidenr=0, lcoord=None):
        """Find the next element in the model, given the acutal
        element |actelem| and either
        o the side number |sidenr|
        o the local coordinats |lcoord| (one component is > 0)
        returns the next element or None"""
        # new shortcut with the neighbors index
        if lcoord is None:
            if isinstance(actelem, Element.Element):
                return self.neighborSideIndex[actelem.name][sidenr]
            else:
                return self.neighborSideIndex[actelem][sidenr]
            

        # get the nodes on the side, in lcoord
        nextnodes = actelem.shape.nextPattern(lcoord)
        # loop over the nodes in the list of the next nodes
        elems = dict()
        # find all elements that share the nextnodes
        for n in nextnodes:
            for k in self.nodeIndex[actelem.nodes[n]]:
                elems[k] = elems.setdefault(k, 0) + 1
        # delete the present element from that list
        elems.pop(actelem.name)

        # find the element with the same number of nodes on that side
        # was soll das. falsch!!!
        for e,v in elems.items():
            if v == len(nextnodes):
                return self.getElement( e )
        return None

    def getElement(self, elename):
        """Create an element instance from the connectivity information
        (the Element class holds a list of nodes)
        |elem| is the name (i.e. the number) of the element
        (This used to be ElementFactory)
        """
        # test whether the element already exists, shortcut if exists
        try:
            return self.elementCache[elename]
        except:
            pass
        conn = self.Conn[elename]
        nodes = conn[1]
        try:
            sh = self.shapeCache[conn[0]]
        except:
            sh = shapeFunctions[conn[0]]()
            self.shapeCache[conn[0]] = sh

        # collect the nodal coordinates
        nodcoord = np.array([self.Coord[n] for n in nodes])

        # invoke an instance of the element
        e = Element.Element( nodes, sh, nodcoord, elename )
        self.elementCache[elename] = e
        return e

    def getAllCoordinatesAsArray(self):
        """Collect all coordinates and return them as numpy array
        """
        return np.array(list(self.Coord.values()))

    def getVariables(self, element, nodvars=None, intpointvars=None, deriv=False):
        """Get a variable from the model for the |element| given
        The local coordinate of the |element| must be set to the correct value
        (as is the case after a findElement(point) or after explicitly
        setting through element.setLocalCoord(lcoord)).

        |nodvars| is the list of variable names defined on the nodes
        |intpointvars| is the list of variable names defined on the integration points

        The names of the variables can be queried through
        getNodVarInfo and getIntPointVarInfo
        """

        resNodvars, resIntPointVars = [], []

        # find the appropriate nodal variables
        if nodvars:
            for postvar in nodvars:
                idx = self.NodVarInfo.index(postvar)
                nodvar = []
                for node in element.nodes:
                    nodvar.append( self.NodVar[node][idx] )
                resNodvars.append( nodvar )
            resNodvars = np.asarray(resNodvars, dtype=np.float_)
            resNodvars = element.mapNodalVar(resNodvars, deriv=deriv)

        # find the appropriate integration point variable
        if intpointvars:
            idx = []
            resIntPointVars = self.IntPointVar[element.name]
            for postvar in intpointvars:
                idx.append(self.IntPointVarInfo.index(postvar))
            resIntPointVars = np.take( resIntPointVars, idx, 1 )
            resIntPointVars = element.mapIntPointVar(resIntPointVars, deriv=deriv)

        return resNodvars, resIntPointVars

    def getNodVar(self, point, postvars, deriv=False, accuracy=1.49012e-8):
        """Find the values of the variables |postvars| at
        the global coordinate |point|
        use |accuracy| to locate the points
        """
        point = np.asarray(point, dtype=np.float_)
        element = self.findElement(point, accuracy)
        # check wether the point is within the model
        if not element:
            return None
        resNodvars, resIntPointVars = self.getVariables(element, nodvars=postvars, deriv=deriv)
        return resNodvars

    def getIntPointVar(self, point, postvars, deriv=False):
        """Find the values of the integration point variables |postvars| at 
        the global coordinate |point|
        """
        point = np.asarray(point, dtype=np.float_)
        element = self.findElement(point)
        # check wether the point is within the model
        if not element:
            return None
        resNodvars, resIntPointVars = self.getVariables(element, intpointvars=postvars, deriv=deriv)
        return resIntPointVars

    def getAdjacentNodes(self, node, onboundary=False):
        """Get the nodes that are connected to |node| on an edge.
        If |node| is not a element corner node return None
        (this should happen only for higher order elements).
        """
        nextnodes = set()
        for elem in self.nodeIndex[node]:
            e = self.getElement(elem)
            enodes = np.asarray(e.nodes)
            if not node in enodes[e.shape.cornernodes]:
                return None
            nextnodes.update(enodes[e.shape.nextnodes[enodes == node]][0].tolist())
        if onboundary:
            nextnodes = set(self.findBoundaryNodes().keys()).intersection(nextnodes)
        return list(nextnodes)

    def findBoundaryNodes(self, recalc=False):
        """Find all the nodes on the model boundary.

        Returns a dict with node names as keys.

        The values are dicts with the elements that contain the node,
        and a list with the boundary sides of this element.
        """
        if not self.boundaryNodes or recalc:
            belems = self.findBoundaryElements(recalc=recalc)
            bnodes = {}
            for elename, sides in belems.items():
                e = self.getElement(elename)
                for side in sides:
                    nodes = np.array(e.nodes)[e.shape.sidenodes[side]]
                    for n in nodes:
                        bnodes.setdefault(n, {}).setdefault(elename, list()).append(side)
            self.boundaryNodes = bnodes
        return self.boundaryNodes

    # def findBoundarySides(self, recalc=False):
    #     """Find all the nodes on the model boundary.

    #     Returns a dict with side numbers as keys.

    #     The values are lists with the node names that live on the sides.
    #     """
    #     # find the nodes on the boundary sides
    #     belems = self.findBoundaryElements(recalc=recalc)
    #     bsides = {}
    #     for elename, sides in belems.items():
    #         e = self.getElement(elename)
    #         for side in sides:
    #             nodes = np.array(e.nodes)[e.shape.sidenodes[side]]
    #             bsides.setdefault(side, set()).update(set(nodes))
    #     return bsides

    def findBoundaryElements(self, side=None, recalc=False):
        """Find all the elements on the model boundary. If |side| is given,
        find only those parts of the boundary that consist of a given element
        side (in the element local system; note that this might only be useful
        for regular quad grids).

        Returns a dict, where the keys are the element names and the values a
        list of all boundary sides
        """
        return self.boundaryElems

    def findModelBoundary(self, point, direction, savebmodel=True, culling=True):
        """Find the boundary of the model in |direction|, starting at the
        given |point|.  Return None if no boundary is found.
        |direction| and point| should be arrays, lists or tuples of the same
        |dimension as the model.  """

        #direction = direction / np.sqrt(np.dot(direction,direction))
        point = np.asarray(point)
        direction = np.asarray(direction)
        
        try:
            bmodel = self.bmodel_tri
        except:
            bmodel = self.extractBoundaryModel(triangles=True)
            if savebmodel:
                self.bmodel_tri = bmodel
        
        import intersect_triangle
        for ele in bmodel.getElementNames():
            e = bmodel.getElement(ele)
            res = intersect_triangle.intersect_triangle(point, direction, e.nodcoord)
            if res != None:
                # res also contains distance and local variables
                return res[-1]
        return None
            
                
        

    def findModelBoundaryOld(self, point, direction, accuracy=0.0001):
        """Find the boundary of the model in |direction|, starting at the
        given |point|.  Return None if no boundary is found.
        |direction| and point| should be arrays, lists or tuples of the same
        |dimension as the model.  """

        point, direction = np.asarray(point, dtype=np.float_), np.asarray(direction, dtype=np.float_)

        direction = direction / np.sqrt(np.dot(direction,direction))
        
        # set the accuracy to the given value
        elem_tol, elem_btol = self.elem_tol, self.elem_btol
        self.elem_tol, self.elem_btol = accuracy, accuracy
        
        e = self.findElement(point)
        # if we start outside the model: return find the next Element inside
        # the model
        if not e:
            #             node = self.getCoordinate( self.findClosestNode(point) )
            #             dist = np.sqrt(np.sum(node-point)**2)
            #             pt = point + dist*dir
            return None

        acc = 1.0
        while acc > accuracy:
            e = self.findElement(point)
            if not e:
                return None

            pt = point
            # find the boundary
            while 1:
                # dcoord is the half-size of the element in global coordinates
                # i.e. 0.5*(dx, dy, ..)
                #dcoord = np.sum(np.dot( e.shape.calcShapeDeriv(np.zeros(e.dim)), e.nodcoord))
                dcoord = np.sqrt(np.sum((e.nodcoord[1]-e.nodcoord[0])**2))/5.
                pt = point + acc*direction*abs(dcoord)
                if self.verbose:
                    print('element: ', e.name , ';  point  : ', pt)
                ee = self.findElement(pt)
                if ee:
                    point = pt
                    e = ee
                else:
                    break

            # now we should be in some boundary element (probably not yet the correct..)
            lcoord = e.lcoord
            pt = point + acc*direction*abs(dcoord)
            lc = e.findLocalCoord(pt)
            if isInRange(lc, 1.):
                e.lcoord = lc
                point = pt
            else:
                e.lcoord = lcoord
                fact = max(2.5, max(lc)/2.5)
                acc = acc/fact  # 2.5 seems to be quite efficient
        # reset the tolerances
        self.elem_tol, self.elem_btol = elem_tol, elem_btol

        # go the exact element boundary
        e.lcoord = np.clip(e.lcoord, -1., 1.)
        return e.mapNodalVar(np.transpose(e.nodcoord))

    def sweepNodes(self, accuracy=1.e-12, nosweep=False):
        """Remove duplicate nodes, throw away unused nodes, update the
        connectivity and the set definition

        |accuracy| gives the accuracy of the node coordinates

        This is a quite slow, but correct implementation.
        """
        self.makeModelCache()
        # remove all nodes (Coordinates) not in the index,
        # i.e. that are not part of an element connectivity
        removenodes = []
        for n in self.Coord:
            if not n in self.nodeIndex:
                removenodes.append(n)
        for n in removenodes:
            del self.Coord[n]

        if nosweep:
            self.makeModelCache()
            return
                
        # make a list of duplicate nodes
        nodecoords = np.array(list(self.Coord.values()))
        nodenames  = np.array(list(self.Coord.keys()))
        samenodes  = dict()
        removenodes = set()
        for name, coord in zip(nodenames, nodecoords):
            idx = (((coord- nodecoords)**2).sum(1) < accuracy)
            nnames = nodenames[idx]
            for n in nnames:
                samenodes[n] = nnames[0]
            for n in nnames[1:]:
                removenodes.add(n)

        # remove unneded nodes
        for n in removenodes:
            self.Coord.pop(n)

        # update the connectivity information 
        for elem in self.Conn:
            etype, conn = self.Conn[elem]
            conn = [samenodes[n] for n in conn]
            self.Conn[elem] = (etype, conn)

        self.makeModelCache()

    def rotate(self, phi, theta, psi):
        """rotate the mesh by the Euler angles
        |phi|, |theta|, |psi| should be given in degrees
        after this the update method should be called
        """
        p = -phi/180.*np.pi
        t = -theta/180.*np.pi
        s = -psi/180.*np.pi
        sp, cp = np.sin(p), np.cos(p)
        st, ct = np.sin(t), np.cos(t)
        ss, cs = np.sin(s), np.cos(s)

        for node, coord in self.Coord.items():
            if len(coord) == 3:
                x,y,z = coord
            elif len(coord) == 2:
                x,y = coord
                z   = 0.
            self.Coord[node] = np.array((
                ( cp*cs-sp*ct*ss)*x + ( sp*cs+cp*ct*ss)*y + (st*ss)*z,
                (-cp*ss-sp*ct*cs)*x + (-sp*st+cp*ct*cs)*y + (st*cs)*z,
                ( sp*st)*x          + (-cp*st)*y          + (ct)*z   ))

    def translate(self, dx):
        """translate the mesh by the amount |dx|
        which should be given as array of the correct length
        after this the update method should be called
        """
        dd = np.asarray(dx)
        for node, coord in self.Coord.items():
            self.Coord[node] = self.Coord[node] + dd

    def scale(self, factor):
        """scale the mesh with |factor|
        this just multiplies all coordinate values by |factor|
        """
        for node, coord in self.Coord.items():
            self.Coord[node] = self.Coord[node]*factor

    def extractBoundaryModel(self, triangles=False, faces=None):
        """extract a model that only contains the boundary nodes and sides
        if |faces| is given, only extract the elements with the given faces
        """
        bmodel = FEModel()
        belems = self.findBoundaryElements()

        for elename, sides in belems.items():
            e = self.getElement(elename)
            faces = faces or range(e.shape.nsides)
            for s in sides:
                if s in faces:
                    nodes = np.asarray(e.nodes)
                    shape = e.shape
                    if triangles:
                        for i, tri in enumerate(shapeFunctions[shape.sidetype].triangles):
                            bmodel.setElement('%s-%elename-%d' % (str(elename), s, i), 'Tri3',
                                              nodes[shape.sidenodes[s]][tri])
                    else:
                        bmodel.setElement('%s-%elename' % (str(elename), s), shape.sidetype,
                                          nodes[shape.sidenodes[s]])
                    for c in nodes[shape.sidenodes[s]]:
                        bmodel.setCoordinate(c, self.getCoordinate(c))
        #bmodel.renumberElements()
        bmodel.update()
        return bmodel

def TestFind():
    """Test function, mainly for profiling purposes"""
    for i in range(50):
        for n in m.getCoordinateNames():
            ci = m.getCoordinate(n)
            point = ci+np.array([0.0123,-0.0234])*i
            m.getNodVar(point,['v_x','f_y', 'v_z'])


def TestFindModelBoundary():
    """Test function, mainly for profiling purposes"""
    for c in m.Coord.items():
        coo = m.findModelBoundary( np.array([c[1][0],0.01]),
                                   np.array([1,-1]), accuracy=0.00001 )


# Tests
if __name__ == "__main__":

    # from feval.fecodes.exodusII.ExodusIIFile import ExodusIIFile
    # write a mesh with nodvars


    # import feval.fecodes.xdr.LibmeshFile as lm
    # print('init')
    # m = FEModel()
    # mf = lm.LibmeshFile(m)
    # print('read')
    # mf.readMesh('/home/tinu/projects/fismo/mesh/D7000_2nd.xda')
    # stop

    import fecodes.gmv.GMVFile as gmv
    m = FEModel()
    mf = gmv.GMVFile(m)
    mf.readFile('../examples/mesh.gmv')
    # surfmodel = FEModel()
    # mf = gmv.GMVFile(surfmodel)
    # mf.readFile('../../fismo/mesh/surfsystem.gmv')
    # c = np.array([ -1.95500000e+02,   2.25000000e+03,   4.28571429e-01])

    m.sweepNodes()
    m.renumberNodes(10)
    for n, c in list(m.Coord.items()):
        m.setCoord(n-200, c)
    m.update()
    m.renumberNodes(10)
    m.sweepNodes()
    m.update()
    m.renumberNodes(1)
    m.update()

    stop

    # from fecodes.marc.MarcPost import *
    # from fecodes.marc.MarcFile import *

    # m = FEModel(verbose=1)
    # mf = MarcFile(m)
    # mf.readFile('/soft/numeric/feval/data/marc/test1.dat')

#     bednodes = m.Sets['node']['surf']    
#     elems = m.findElementsFromNodes(bednodes)
#     stop

#     print('loading the model')
#     ## 2D-model test case
#     m = FEModel(verbose=1)
#     mf = MarcPostFile(m, '../data/marc/e7x1b.t16')
#     mf.readInc(1)

#     for n, c in m.Coord.items():
#         m.setNodVar(n, [c[0]*0.5, c[1]*2.])
#     m.setNodVarInfo(['X','Y'])
    
#     point = np.array([7,10,0])
#     print(m.findElement(point).name)
#     print(m.getNodVar(point, m.NodVarInfo))
#     x =  m.getNodVar(point+np.array([1.0,0.1,0]), m.NodVarInfo, deriv=True)
#     print(m.getNodVar(point, m.NodVarInfo, deriv=True))

#     print(m.getAdjacentNodes(2))
#     #a = m.findBoundaryElements()

#     #     bmodel = m.extractBoundaryModel()
#     #     stop

#     m.setCoordinate(777, m.Coord[7])
#     m.Conn[23][1][3] = 777
#     m.update()

#     m.makeModelCache()
#     m.sweepNodes()
#     stop

#     e = m.findClosestElement([20, 20])
#     ee = m.findClosestElement([0, 0])

#     print(m.findNextElement(e, lcoord = [0, -1]).name)
#     print(m.findNextElement(e, 0).name)

#     stop

#     a = m.findBoundaryElements()
#     stop

#     point = m.getCoordinate(m.getCoordNames()[2])+np.array([-0.13,0.021])
#     print(m.getNodVar(point,['d_x','d_y']))
#     point = m.getCoordinate(m.getCoordNames()[2])+np.array([-0.13,10.021])
#     boundary = m.findModelBoundary(point, np.array([5,-5]), accuracy=0.0000001)
#     print('the boundary is at ', boundary)

# #     print(m.getIntPointVar(point,['t']))
 

#     stop

    # 3D-model
    try:
        m
    except:
        m  = FEModel()
        mf = MarcPostFile(m, '/home/tinu/projects/colle/marc/cgx8_dc0.80/cgx8dec.t16')
        mf.readInc(1)

    print(m)

    point = m.getCoordinate(1001)+np.array([-10,10,-50])
    direction = np.array([0,0,1])
    print(m.findModelBoundaryOld(point, direction, accuracy=0.001))
    print('no culling')
    print(m.findModelBoundary(point, direction, culling=False))
    print('culling')
    print(m.findModelBoundary(point, direction, culling=True))

    print(m)
    m.bmodel_tri.renumberNodes(1)
    m.bmodel_tri.renumberElements(1)
    print(m)
    m.bmodel_tri.update()
    print(m)

    import feval.fecodes.gmv.GMVFile as gmv
    gf = gmv.GMVFile(m.bmodel_tri)
    gf.setWrite('gmvinput')
    gf.setWrite('nodes')
    gf.setWrite('cells')
    gf.setWrite('endgmv')
    gf.writeFile('A.gmv')




### profiling

#     print('testing')
#     import profile

#     m.verbose=0
#     m.makeModelCache()
#     # profile.run('TestFind()')

#     psyco.bind(Element)
#     psyco.bind(FEModel)
#     psyco.bind(ShapeFunctions.ShapeFunction_Quad8)

#     m.makeModelCache()
#    profile.run('TestFindModelBoundary()')
# def TestFindModelBoundary():
#     """Test function, mainly for profiling purposes"""
#     for c in m.Coord.items():
#         coo = m.findModelBoundary( np.array([c[1][0],0.01]),
#                                    np.array([1,-1]), accuracy=0.00001 )


# # Tests
# if __name__ == "__main__":
#     from feval.fecodes.marc.MarcT16File import *
#     from feval.fecodes.marc.MarcFile import *

#     m = FEModel(verbose=1)
#     mf = MarcFile(m)
#     #mf.readFile('data/marc/test1.dat')
#     mf.readFile('/soft/numeric/feval/data/marc/test1.dat')

#     bednodes = m.Sets['node']['surf']    
#     elems = m.findElementsFromNodes(bednodes)
#     stop

#     print('loading the model')
#     ## 2D-model test case
#     m = FEModel(verbose=1)
#     mf = MarcT16File(m, '../data/marc/e7x1b.t16', verbose=1)
#     mf.readInc(1)

#     point = m.getCoordinate(m.getCoordNames()[2])+np.array([-0.13,0.021])
#     print(m.getNodVar(point,['d_x','d_y']))
#     point = m.getCoordinate(m.getCoordNames()[2])+np.array([-0.13,10.021])
#     boundary = m.findModelBoundary(point, np.array([5,-5]), accuracy=0.0000001)
#     print('the boundary is at ', boundary)
# #     print(m.getIntPointVar(point,['t']))



#     ## 3D-model
# #     try:
# #         mf
# #     except:
# #         m = FEModel()
# #         m.verbose=1
# #         m.accuracy = 1.e-20
# #         ##mf = MarcT16File(m, '/home/tinu/numeric/feval/test/mm_22_51_22_3d_mid_g0.0-n1-a5.3.t16')
# #         mf = MarcT16File(m, '/home/tinu/projects/jako/marc/jako3db_polythermal.t16')
# #         mf.readInc(1)
# #     point = m.getCoordinate(m.getCoordinateNames()[1500])+np.array([-0.5,0.05,0.02])

# #     print('nodvar ', m.getNodVar(point,['d_x','d_y']))
# #     print('point accuracy: ', m.getIntPointVar(point,['x','y','z'])-point)
# #     print(m.getIntPointVar(point,['sigma_6']))
# #     print(m.findModelBoundary(point, np.array([0,-5,-6]), accuracy=0.01))

# ### profiling

#     print('testing')
#     import profile

#     m.verbose=0
#     m.makeModelCache()
#     # profile.run('TestFind()')

# #     psyco.bind(Element)
# #     psyco.bind(FEModel)
# #     psyco.bind(ShapeFunctions.ShapeFunction_Quad8)

#     m.makeModelCache()
# #    profile.run('TestFindModelBoundary()')

#     import feval.fecodes.gmv.GMVFile as gmv
#     datapath = '/home/tinu/fismo/trunk/visco3d/model-runs'
#     filename = 'with-tongue,n=1,etab=500,etaff=0.1.gmv'
#     m = FEModel()
#     m.verbose=0
#     m.accuracy = 1.e-20
#     mf = gmv.GMVFile(m)
#     mf.readFile(datapath+'/'+filename)
#     m.removeUnusedNodes()
    
#     for n, c in m.Coord.items():
#         m.setNodVar(n, c)
#     m.setNodVarInfo(['X', 'Y', 'Z'])
    
#     point = np.array([5000,100,0])
#     print(m.findElement(point).name)
#     print(m.getNodVar(point, m.NodVarInfo, deriv=True))

