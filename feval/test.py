import feval.ShapeFunctions

sh6 = feval.ShapeFunctions.ShapeFunction_Tri6()

def shape(zeta):
    zeta1, zeta2 = zeta
    zeta0 = 1. - zeta1 - zeta2
    return [2.*zeta0*(zeta0-0.5),
           2.*zeta1*(zeta1-0.5),
           2.*zeta2*(zeta2-0.5),
           4.*zeta0*zeta1,
           4.*zeta1*zeta2,
           4.*zeta2*zeta0]


print(shape([0.,0.]))
print(sh6([0.,0.]))
print('----------------------')
print(shape([1.,0.]))
print('----------------------')
print(shape([0.,1.]))
print('----------------------')
print(shape([0.5,0.5]))
print('----------------------')
print(shape([0.5,0.5]))
print('----------------------')

