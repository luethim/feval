# make a boring quad mesh

import numpy as N
import feval.FEval

def makeSquare(model, xmin, xmax, ymin, ymax, nx, ny):
    """creates a sqare mesh of nx*ny elements
    """
    model.dim = 2
    for i in range(nx+1):
        for j in range(ny+1):
            model.setCoordinate('%d,%d' %(i,j), [xmin + (xmax-xmin)/float(nx)*i,
                                                 ymin + (ymax-ymin)/float(ny)*j])
            if i < nx and j < ny:
                corners = ['%d,%d' % (i  ,j  ),
                           '%d,%d' % (i+1,j  ),
                           '%d,%d' % (i+1,j+1),
                           '%d,%d' % (i  ,j+1),
                           ]
                model.setElement('%d,%d' %(i,j), 'Quad4', corners)

def test():
    m = feval.FEval.FEModel()
    makeCube(m,
             0., 1.,
             0., 2.,
             20, 20)
    

if __name__ == '__main__':
    m = feval.FEval.FEModel()
    makeSquare(m,
               0., 1.,
               0., 2.,
               20, 20)

    m.renumberNodes()
    m.renumberElements()

    import feval.fecodes.gmv.GMVFile as gmv
    mf = gmv.GMVFile(m)

    mf.setWrite('gmvinput')
    mf.setWrite('nodes')
    mf.setWrite('cells')
    mf.setWrite('variable')
    mf.setWrite('endgmv')

    mf.writeFile('adaptive.gmv.000')
    
